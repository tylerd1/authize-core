## authize

### the simplest authorization server, ever.

A simple, configurable authorization server.
Built on basic php, you can just upload, configure, and activate it and it does the rest.

With a plugin system allowing you to use any protocol, just upload a plugin, and reload the server.
With the most lightweight system, the bare install takes only 2 minutes to start the API.

A simple yet powerful API allows you to write your own plugins to integrate with your applications.